# cssmin

Command-line utility to removes line breaks, comments and spaces
around symbols in a CSS file. Without any dependency, you only need
lua (>= 5.1).

Use a CSS file as argument or CSS block by stdin:

* `./cssmin styles.css > styles.min.css`
* `./cssmin < styles.css > styles.min.css`
* `cat styles.css | ./cssmin`
* `echo ".red { color: red; }" | ./cssmin`

[![asciicast](https://asciinema.org/a/598675.svg)](https://asciinema.org/a/598675)

---

To have access to `cssmin` from everywhere, you can create a symlink
in `$HOME/.local/bin/` or in `/usr/local/bin/`.